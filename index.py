#Bibliotecas#
import paho.mqtt.subscribe as subscribe
import time
from flask import Flask,request, render_template, jsonify
import sys
from random import randrange, uniform



consumo = 0
hora = 1
valor = 0
# instancia o flask
app = Flask(__name__)

# define uma rota, neste caso a rota raiz
@app.route('/Projeto', methods=['GET', 'POST'])
def Monitora():

    global hora
    global valor
    global consumo
    
    msg = subscribe.simple("iot/tensao", hostname="iot.eclipse.org", msg_count=1) #se inscreve no server onde o node manda as infos
    msg2 = subscribe.simple("iot/corrente", hostname="iot.eclipse.org", msg_count=1)
    tensao = int(msg.payload.decode('utf8')) #decodifica a mensagem do node
    corrente = round(((int(msg2.payload.decode('utf8')))/1000),2)
    potencia = round((tensao * corrente),2)
    consumo  += round(((potencia * hora)/1000),2)
    valor +=  round((0.48 * consumo),2)
    hora += 1

    if request.method == 'POST':

        return jsonify(
            variavel=str(tensao),
            variavel2=str(corrente),
            variavel3=str(potencia),
            variavel4=str(consumo),
            variavel5=str(valor),
            variavel6=hora
        )

    else:
    
        return render_template(
            'index.html',
            variavel=str(tensao),
            variavel2=str(corrente),
            variavel3=str(potencia),
            variavel4=str(consumo),
            variavel5=str(round(valor,2)),
            variavel6=hora
        ) #mostra o template html com a variavel no local host

if __name__ == "__main__":
    app.run(debug="True",host='172.18.39.37')
